package com.example.practica3appperros.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.practica3appperros.databinding.ActivityMainBinding


var mContext: Context? = null
var mAppCompatActivity: AppCompatActivity? = null
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mContext = this;
        mAppCompatActivity = this

        setSupportActionBar(binding.toolbar)

    }

}