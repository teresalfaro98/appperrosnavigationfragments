package com.example.practica3appperros.fragments

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.navigation.fragment.findNavController
import com.example.practica3appperros.R
import com.example.practica3appperros.databinding.FragmentDetailBinding
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import com.example.practica3appperros.activities.mAppCompatActivity
import com.example.practica3appperros.activities.mContext

class DetailFragment : Fragment(R.layout.fragment_detail)  {

    private var _binding:FragmentDetailBinding? = null
    private val binding get() = _binding !!

    val args:DetailFragmentArgs by navArgs() //Delegation

    private var imgUrl:String? = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgUrl = args.imgUrl
        Picasso.get().load(imgUrl).into(binding.imgPerrote);



        mAppCompatActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setHasOptionsMenu(true);

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().navigate(R.id.action_detailFragment_to_listFragment)

        }
        return super.onOptionsItemSelected(item)
    }

}