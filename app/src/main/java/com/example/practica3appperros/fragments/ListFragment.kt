package com.example.practica3appperros.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.practica3appperros.R
import com.example.practica3appperros.activities.mAppCompatActivity
import com.example.practica3appperros.activities.mContext
import com.example.practica3appperros.adapters.DogAdapter
import com.example.practica3appperros.databinding.FragmentListBinding
import com.example.practica3appperros.http.ApiInterface
import com.example.practica3appperros.models.Dog
import com.example.practica3appperros.models.MessageResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ListFragment : Fragment(R.layout.fragment_list) {

    private var _binding:FragmentListBinding? = null
    private val binding get() = _binding !!

    private var baseURL = "https://dog.ceo/api/"
    private lateinit var dogRecyclerView: RecyclerView
    private lateinit var dogAdapter: DogAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListBinding.inflate(inflater,container,false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dogRecyclerView = binding.recyclerDogs
        dogRecyclerView.setHasFixedSize(true)
        dogRecyclerView.layoutManager = LinearLayoutManager(mContext)

        mAppCompatActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(false)

        getData()

    }

    private fun setAdapter(doglist: MutableList<Dog>){
        dogAdapter = DogAdapter(doglist){  dog->
           Toast.makeText(mContext,dog.name,Toast.LENGTH_SHORT).show()

            findNavController().navigate(R.id.action_listFragment_to_detailFragment,bundleOf(
                "imgUrl" to dog.imageURL
            ))
        }
        dogRecyclerView.adapter = dogAdapter

    }


    fun getData(){
        val lst : MutableList<Dog> = mutableListOf()

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseURL)
            .build()
            .create(ApiInterface::class.java)

        val builder = retrofitBuilder.getDogs()


        builder.enqueue(object: Callback<MessageResponse?> {
            override fun onResponse(
                call: Call<MessageResponse?>,
                response: Response<MessageResponse?>
            ) {
                Log.d("DOG","Here")
                val responseBody = response.body()!!
                for(imageUrl in responseBody.message)
                {
                    lst.add(Dog(imageUrl,imageUrl))
                }
                setAdapter(lst)
            }
            override fun onFailure(call: Call<MessageResponse?>, t: Throwable) {
                Log.d("DOG","Error")
            }
        })

    }

}